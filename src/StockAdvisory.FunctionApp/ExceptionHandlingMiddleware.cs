﻿using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Middleware;
using Microsoft.Extensions.Logging;
using StockAdvisory.BusinessLayer.Requests;

namespace StockAdvisory.FunctionApp
{
    public class ExceptionHandlingMiddleware(ILogger<ExceptionHandlingMiddleware> logger)
        : IFunctionsWorkerMiddleware
    {
        private readonly ILogger<ExceptionHandlingMiddleware> _logger = logger;

        public async Task Invoke(FunctionContext context, FunctionExecutionDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An unhandled exception occurred.");
                context.GetInvocationResult().Value = new RequestResponse(ex);
            }
        }
    }
}
