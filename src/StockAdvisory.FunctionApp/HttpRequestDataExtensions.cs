﻿using System.Net;
using Microsoft.Azure.Functions.Worker.Http;
using StockAdvisory.BusinessLayer.Models;

namespace StockAdvisory.FunctionApp;

public static class HttpRequestDataExtensions
{
    public static HttpResponseData CreateFileResponse(
        this HttpRequestData request,
        Stream stream,
        ExportType exportType,
        string fileName
    )
    {
        var fileExtension = exportType.GetExportFileExtension();
        var response = request.CreateResponse(HttpStatusCode.OK);
        response.Headers.Add("Content-Type", $"text/{fileExtension}");
        response.Headers.Add("Content-Length", stream.Length.ToString());
        response.Headers.Add(
            "Content-Disposition",
            $"attachment; filename={fileName}.{fileExtension}"
        );
        stream.CopyTo(response.Body);
        return response;
    }
}
