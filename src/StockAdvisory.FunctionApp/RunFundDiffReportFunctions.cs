using System.Net;
using Mediator;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Http;
using StockAdvisory.BusinessLayer.Requests;

namespace StockAdvisory.FunctionApp;

public class RunFundDiffReportFunctions(IMediator mediator)
{
    [Function(nameof(DownloadCurrentHoldings))]
    public async Task<IActionResult> DownloadCurrentHoldings(
#pragma warning disable RCS1163 // Unused parameter
        [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "download/{fileName}")]
#pragma warning disable IDE0060 // Remove unused parameter
        HttpRequest request,
#pragma warning restore IDE0060 // Remove unused parameter
#pragma warning restore RCS1163 // Unused parameter
        string fileName,
        string? url = null
    )
    {
        var result = await mediator.Send(new DownloadHoldingsRequest(fileName, url));
        if (result.HasError)
        {
            return new ObjectResult(result) { StatusCode = result.ErrorCode };
        }
        return new OkResult();
    }

    [Function(nameof(CreateDiffReport))]
    public async Task<HttpResponseData> CreateDiffReport(
        [HttpTrigger(AuthorizationLevel.Anonymous, "post")] HttpRequestData request,
        [Microsoft.Azure.Functions.Worker.Http.FromBody] RunDiffReportRequest reportRequest
    )
    {
        var result = await mediator.Send(reportRequest);
        if (result.HasError)
        {
            return request.CreateResponse((HttpStatusCode)result.ErrorCode);
        }
        return request.CreateFileResponse(result.Result!, reportRequest.ExportType, "holdings");
    }
}
