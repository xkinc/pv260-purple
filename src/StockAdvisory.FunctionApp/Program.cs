using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using StockAdvisory.BusinessLayer;
using StockAdvisory.BusinessLayer.Services;
using StockAdvisory.FunctionApp;

var host = new HostBuilder()
    .ConfigureFunctionsWorkerDefaults(workerApp =>
        workerApp.UseMiddleware<ExceptionHandlingMiddleware>()
    )
    .ConfigureServices(
        (builder, services) =>
        {
            services.AddBusinessLayerServices(builder.Configuration);
            services.Configure<EmailConfiguration>(c =>
            {
                c.PortNumber = 587;
                c.SmtpAddress = Environment.GetEnvironmentVariable("SmtpAddress")!;
                c.SenderEmail = Environment.GetEnvironmentVariable("SenderEmail")!;
                c.SenderPassword = Environment.GetEnvironmentVariable("SenderPassword")!;
                c.Subject = Environment.GetEnvironmentVariable("Subject")!;
                c.Body = Environment.GetEnvironmentVariable("Body")!;
                c.EnableSSL = "true";
            });
            services.Configure<FundSettings>(c =>
            {
                c.HoldingsUrl = Environment.GetEnvironmentVariable("HoldingsUrl")!;
                c.DefaultRequestHeader = Environment.GetEnvironmentVariable(
                    "DefaultRequestHeader"
                )!;
            });
        }
    )
    .Build();

host.Run();
