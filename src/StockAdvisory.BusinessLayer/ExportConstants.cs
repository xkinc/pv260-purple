﻿namespace StockAdvisory.BusinessLayer;

public static class ExportConstants
{
    public const string IncreasedPositionsHeader = "Increased Positions:";
    public const string DecreasedPositionsHeader = "Decreased positions:";
    public const string NewPositionsHeader = "New positions:";
}
