﻿namespace StockAdvisory.BusinessLayer.Exceptions;

/// <summary>
/// an exception that is thrown when the parsing of the fund data failed
/// likely reason is that the data is not in the expected format
/// </summary>
public class ParsingException : Exception
{
    public ParsingException() { }

    public ParsingException(string? message)
        : base(message) { }

    public ParsingException(string? message, Exception? innerException)
        : base(message, innerException) { }
}
