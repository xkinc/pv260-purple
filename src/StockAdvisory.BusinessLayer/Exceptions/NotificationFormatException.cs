﻿namespace StockAdvisory.BusinessLayer.Exceptions;

/// <summary>
/// an exception that is thrown when notification format is invalid
/// </summary>
public class NotificationFormatException : NotificationException
{
    public NotificationFormatException(string? message)
        : base(message) { }

    public NotificationFormatException()
        : base() { }

    public NotificationFormatException(string? message, Exception? innerException)
        : base(message, innerException) { }
}
