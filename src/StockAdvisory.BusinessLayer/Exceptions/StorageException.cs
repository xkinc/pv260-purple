﻿namespace StockAdvisory.BusinessLayer.Exceptions;

/// <summary>
/// an exception that is thrown when fund holding is unable to be stored or retrieved
/// </summary>
public class StorageException : Exception
{
    public StorageException() { }

    public StorageException(string? message)
        : base(message) { }

    public StorageException(string? message, Exception? innerException)
        : base(message, innerException) { }
}
