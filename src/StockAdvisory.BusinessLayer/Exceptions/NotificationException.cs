﻿namespace StockAdvisory.BusinessLayer.Exceptions;

/// <summary>
/// an exception that is thrown when there is an issue while sending a notification
/// </summary>
public class NotificationException : Exception
{
    public NotificationException() { }

    public NotificationException(string? message)
        : base(message) { }

    public NotificationException(string? message, Exception? innerException)
        : base(message, innerException) { }
}
