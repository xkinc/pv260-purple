﻿namespace StockAdvisory.BusinessLayer.Exceptions;

/// <summary>
/// an exception that is thrown when the fund holding data is unable to be downloaded
/// </summary>
public class HoldingsDownloaderException : Exception
{
    public HoldingsDownloaderException() { }

    public HoldingsDownloaderException(string? message)
        : base(message) { }

    public HoldingsDownloaderException(string? message, Exception? innerException)
        : base(message, innerException) { }
}
