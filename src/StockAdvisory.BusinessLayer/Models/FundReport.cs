﻿namespace StockAdvisory.BusinessLayer.Models;

public class FundReport
{
    public List<Fund> NewPositions { get; } = [];
    public List<Fund> RemovedPositions { get; } = [];
    public List<FundDiff> IncreasedPositions { get; } = [];
    public List<FundDiff> DecreasedPositions { get; } = [];

    public static FundReport CreateReport(IEnumerable<Fund> oldFunds, IEnumerable<Fund> newFunds)
    {
        var report = new FundReport();

        // Compare based on the fund CUSIP https://www.investopedia.com/terms/c/cusipnumber.asp
        var newFundsDictionary = newFunds.ToDictionary(fund => fund.Cusip);

        foreach (var oldFund in oldFunds)
        {
            if (!newFundsDictionary.TryGetValue(oldFund.Cusip, out var newFund))
            {
                // Fund exists in old but not in new
                report.RemovedPositions.Add(oldFund);
                continue;
            }

            // Unchanged funds are skipped
            if (newFund.Shares == oldFund.Shares)
            {
                newFundsDictionary.Remove(oldFund.Cusip);
                continue;
            }

            var fundDiff = FundDiff.GetFundDiff(oldFund, newFund);
            if (newFund.Shares > oldFund.Shares)
            {
                report.IncreasedPositions.Add(fundDiff);
            }
            else
            {
                report.DecreasedPositions.Add(fundDiff);
            }

            newFundsDictionary.Remove(oldFund.Cusip);
        }

        // Check for funds present in new but not in old
        report.NewPositions.AddRange(newFundsDictionary.Values);

        return report;
    }
}
