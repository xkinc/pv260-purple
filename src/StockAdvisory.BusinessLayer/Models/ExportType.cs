﻿namespace StockAdvisory.BusinessLayer.Models;

public enum ExportType
{
    Csv = 1,
    Html
}

public static class ExportTypeExtensions
{
    public static string GetExportFileExtension(this ExportType exportType) =>
        exportType switch
        {
            ExportType.Csv => "csv",
            ExportType.Html => "html",
            _ => throw new NotSupportedException($"Export type {exportType} is not supported.")
        };
}
