﻿using System.Globalization;

namespace StockAdvisory.BusinessLayer.Models;

public class Fund(
    DateTime date,
    string fundName = "",
    string company = "",
    string ticker = "",
    string cusip = "",
    int shares = 0,
    decimal marketValue = 0,
    decimal weight = 0
)
{
    public DateTime Date { get; } = date;
    public string FundName { get; } = fundName;
    public string Company { get; } = company;
    public string Ticker { get; } = ticker;
    public string Cusip { get; } = cusip;
    public int Shares { get; } = shares;
    public decimal MarketValue { get; } = marketValue;
    public decimal Weight { get; } = weight;

    public override string ToString()
    {
        return $"{Date},{FundName},{Company},{Ticker},{Cusip},{Shares},{MarketValue},{Weight}";
    }

    public static bool TryParseFund(string[] fields, out Fund fund)
    {
        fund = new Fund(DateTime.Now);

        CultureInfo culture = CultureInfo.GetCultureInfo("en-US");

        if (!DateTime.TryParse(fields[0], culture, out var date))
        {
            return false;
        }

        if (
            !int.TryParse(
                fields[5],
                NumberStyles.AllowThousands,
                CultureInfo.InvariantCulture,
                out var shares
            )
        )
        {
            return false;
        }

        var cultureProvider = CultureInfo.CreateSpecificCulture("en-US");
        cultureProvider.NumberFormat.CurrencyDecimalSeparator = ".";
        cultureProvider.NumberFormat.CurrencyGroupSeparator = ",";

        if (
            !decimal.TryParse(
                fields[6],
                NumberStyles.Currency,
                cultureProvider,
                out var marketValue
            )
        )
        {
            return false;
        }

        if (
            !decimal.TryParse(
                fields[7].TrimEnd('%', ' '),
                NumberStyles.AllowDecimalPoint,
                cultureProvider,
                out var weight
            )
        )
        {
            return false;
        }

        fund = new Fund(
            date,
            fields[1],
            fields[2],
            fields[3],
            fields[4],
            shares,
            marketValue,
            weight
        );
        return true;
    }
}
