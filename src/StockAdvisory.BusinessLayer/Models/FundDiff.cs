﻿namespace StockAdvisory.BusinessLayer.Models;

public class FundDiff(
    DateTime date,
    string fundName,
    string company,
    string ticker,
    string cusip,
    int sharesDiff,
    decimal marketValueDiff,
    decimal weightDiff
)
{
    public DateTime Date { get; } = date;
    public string FundName { get; } = fundName;
    public string Company { get; } = company;
    public string Ticker { get; } = ticker;
    public string Cusip { get; } = cusip;
    public int SharesDiff { get; } = sharesDiff;
    public decimal MarketValueDiff { get; } = marketValueDiff;
    public decimal WeightDiff { get; } = weightDiff;

    public static FundDiff GetFundDiff(Fund oldFund, Fund newFund)
    {
        if (newFund.Cusip != oldFund.Cusip)
        {
            throw new ArgumentException("Diff should only be called on the same fund.");
        }

        return new FundDiff(
            newFund.Date,
            newFund.FundName,
            newFund.Company,
            newFund.Ticker,
            newFund.Cusip,
            newFund.Shares - oldFund.Shares,
            newFund.MarketValue - oldFund.MarketValue,
            newFund.Weight - oldFund.Weight
        );
    }

    public override string ToString()
    {
        return $"{Company},{Ticker},{SharesDiff},{WeightDiff}";
    }
}
