﻿using System.Net.Mail;
using StockAdvisory.BusinessLayer.Models;
using StockAdvisory.BusinessLayer.Services.Interfaces;

namespace StockAdvisory.BusinessLayer.Extensions;

public static class ServiceExtensions
{
    public static IExportService GetExportService(
        this ExportType exportType,
        ICollection<IExportService> exportServices
    ) =>
        exportServices.FirstOrDefault(i => i.ExportType == exportType)
        ?? throw new NotSupportedException($"Export type {exportType} is not supported.");

    public static async Task<IEnumerable<Fund>> GetFunds(
        string filePath,
        IStorageService storageService,
        IFundParser fundParser
    )
    {
        await using var csvFile = await storageService.LoadData(filePath);
        return csvFile is null ? [] : fundParser.ParseFundsFromStream(csvFile);
    }

    public static async Task SendNotification(
        this Stream exportStream,
        string[] emails,
        ExportType exportType,
        INotificationSender notificationSender
    )
    {
        using var attachment = new Attachment(
            exportStream,
            $"report.{exportType.GetExportFileExtension()}",
            $"text/{exportType.GetExportFileExtension()}"
        );
        await notificationSender.SendNotificationWithAttachment(emails, attachment);
    }
}
