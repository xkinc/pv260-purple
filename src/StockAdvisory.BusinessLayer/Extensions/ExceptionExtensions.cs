﻿using StockAdvisory.BusinessLayer.Exceptions;

namespace StockAdvisory.BusinessLayer.Extensions;

public static class ExceptionExtensions
{
    public static int GetErrorCode(this Exception e) =>
        e switch
        {
            NotificationFormatException => 400,
            HoldingsDownloaderException => 520,
            StorageException => 521,
            NotificationException => 503,
            NotSupportedException or NotImplementedException => 501,
            _ => 500
        };
}
