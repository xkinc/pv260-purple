﻿namespace StockAdvisory.BusinessLayer.Extensions;

public static class StreamExtensions
{
    public static Stream CreateCopy(this Stream source)
    {
        source.Position = 0;
        var destination = new MemoryStream();
        source.CopyTo(destination);
        destination.Position = 0;
        source.Position = 0;
        return destination;
    }

    public static async Task<Stream> CreateCopyAsync(this Stream source)
    {
        source.Position = 0;
        var destination = new MemoryStream();
        await source.CopyToAsync(destination);
        destination.Position = 0;
        source.Position = 0;
        return destination;
    }
}
