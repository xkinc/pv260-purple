﻿using Mediator;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using StockAdvisory.BusinessLayer.Services.Interfaces;

namespace StockAdvisory.BusinessLayer.Requests;

public record DownloadHoldingsRequest(string SaveFileName, string? HoldingsUrl = null)
    : IRequest<RequestResponse>;

public class DownloadHoldingsRequestHandler(
    IHoldingsDownloader holderDownloader,
    IStorageService storageService,
    IOptions<FundSettings> options,
    ILogger<DownloadHoldingsRequestHandler> logger
) : IRequestHandler<DownloadHoldingsRequest, RequestResponse>
{
    public async ValueTask<RequestResponse> Handle(
        DownloadHoldingsRequest request,
        CancellationToken cancellationToken
    )
    {
        try
        {
            var url = request.HoldingsUrl ?? options.Value.HoldingsUrl;
            await using var newCsv = await holderDownloader.Download(url);
            await storageService.SaveData(
                Configuration.directoryPath + Path.GetFileName(request.SaveFileName),
                newCsv
            );
            logger.LogInformation(
                "{Handler} executed successfully.",
                nameof(DownloadHoldingsRequestHandler)
            );
        }
        catch (Exception e)
        {
            logger.LogError(e, "{Handler} failed.", nameof(DownloadHoldingsRequestHandler));
            logger.LogDebug("{Exception}", e.ToString());
            return new RequestResponse(e);
        }

        return RequestResponse.Success;
    }
}
