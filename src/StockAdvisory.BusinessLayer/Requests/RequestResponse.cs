﻿using StockAdvisory.BusinessLayer.Extensions;

namespace StockAdvisory.BusinessLayer.Requests;

public record RequestResponse
{
    public int ErrorCode { get; protected set; } = 0;
    public string[] ErrorMessages { get; protected set; } = [];
    public bool HasError => ErrorCode != 0;

    public static RequestResponse Success => new();

    protected RequestResponse() { }

    public RequestResponse(Exception Exception)
    {
        ErrorCode = Exception.GetErrorCode();
        ErrorMessages = [Exception.Message];
    }

    public RequestResponse(string errorMessage, int errorCode = 400)
        : this([errorMessage], errorCode) { }

    public RequestResponse(string[] errorMessages, int errorCode = 400)
    {
        ErrorMessages = errorMessages;
        ErrorCode = errorCode;
    }
}

public record RequestResponse<T> : RequestResponse
{
    public T? Result { get; private set; }

    public RequestResponse(T result)
        : base()
    {
        Result = result;
    }

    public RequestResponse(Exception exception)
        : base(exception) { }

    public RequestResponse(string errorMessage, int errorCode = 400)
        : base([errorMessage], errorCode) { }

    public RequestResponse(string[] errorMessages, int errorCode = 400)
        : base(errorMessages, errorCode) { }
}
