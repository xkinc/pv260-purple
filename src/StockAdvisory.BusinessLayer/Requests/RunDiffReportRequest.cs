using Mediator;
using Microsoft.Extensions.Logging;
using StockAdvisory.BusinessLayer.Extensions;
using StockAdvisory.BusinessLayer.Models;
using StockAdvisory.BusinessLayer.Services.Interfaces;

namespace StockAdvisory.BusinessLayer.Requests;

public record RunDiffReportRequest(
    string[] Emails,
    string OldFileName,
    string NewFileName,
    ExportType ExportType
) : IRequest<RequestResponse<Stream>>;

public class RunDiffReportRequestHandler(
    INotificationSender notificationSender,
    IEnumerable<IExportService> exportServices,
    IStorageService storageService,
    IFundParser fundParser,
    ILogger<RunDiffReportRequestHandler> logger
) : IRequestHandler<RunDiffReportRequest, RequestResponse<Stream>>
{
    public async ValueTask<RequestResponse<Stream>> Handle(
        RunDiffReportRequest request,
        CancellationToken cancellationToken
    )
    {
        try
        {
            var result = await ExecuteInternal(
                request.Emails,
                request.OldFileName,
                request.NewFileName,
                request.ExportType
            );
            logger.LogInformation(
                "{Handler} executed successfully.",
                nameof(RunDiffReportRequestHandler)
            );
            return new(result);
        }
        catch (Exception e)
        {
            logger.LogError(e, "{Handler} failed.", nameof(RunDiffReportRequestHandler));
            logger.LogDebug("{Exception}", e.ToString());
            return new RequestResponse<Stream>(e);
        }
    }

    private async Task<Stream> ExecuteInternal(
        string[] emails,
        string OldFileName,
        string NewFileName,
        ExportType exportType
    )
    {
        var exportService = exportType.GetExportService(exportServices.ToArray());

        var oldFunds = await ServiceExtensions.GetFunds(
            Configuration.directoryPath + Path.GetFileName(OldFileName),
            storageService,
            fundParser
        );
        var newFunds = await ServiceExtensions.GetFunds(
            Configuration.directoryPath + Path.GetFileName(NewFileName),
            storageService,
            fundParser
        );
        var report = FundReport.CreateReport(oldFunds, newFunds);

        await using var exportStream = exportService.Export(report);
        var resultStream = exportStream.CreateCopy();
        await exportStream.SendNotification(emails, exportType, notificationSender);

        return resultStream;
    }
}
