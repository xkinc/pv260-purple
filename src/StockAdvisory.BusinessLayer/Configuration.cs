﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StockAdvisory.BusinessLayer.Services;
using StockAdvisory.BusinessLayer.Services.Interfaces;

namespace StockAdvisory.BusinessLayer;

public static class Configuration
{
    public const string directoryPath = "data/download/";

    public static IServiceCollection AddBusinessLayerServices(
        this IServiceCollection services,
        IConfiguration configuration
    )
    {
        services.AddHttpClient();
        services.Configure<FundSettings>(configuration.GetSection("FundSettings"));
        services.Configure<EmailConfiguration>(configuration.GetSection("EmailSettings"));
        CreateDataDirectory();

        services.AddSingleton<IHoldingsDownloader, HoldingsDownloader>();
        services.AddSingleton<IStorageService, FileStorageService>();
        services.AddSingleton<IFundParser, CsvFundParser>();
        services.AddNotifications();
        services.AddRequests();
        services.AddExportServices();

        return services;
    }

    private static void CreateDataDirectory()
    {
        if (!Directory.Exists(directoryPath))
        {
            Directory.CreateDirectory(directoryPath);
        }
    }

    private static IServiceCollection AddNotifications(this IServiceCollection services)
    {
        services
            .AddSingleton<INotificationSender, EmailSender>()
            .AddSingleton<ISmtpClient, SmtpClientWrapper>();
        return services;
    }

    private static IServiceCollection AddExportServices(this IServiceCollection services)
    {
        services
            .AddSingleton<IExportService, CsvExportService>()
            .AddSingleton<IExportService, HtmlExportService>();
        return services;
    }

    private static IServiceCollection AddRequests(this IServiceCollection services)
    {
        services.AddMediator();
        return services;
    }
}
