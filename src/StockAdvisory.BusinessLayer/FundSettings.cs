﻿namespace StockAdvisory.BusinessLayer;

public class FundSettings
{
    public string HoldingsUrl { get; set; } = string.Empty;
    public string DefaultRequestHeader { get; set; } = string.Empty;
}
