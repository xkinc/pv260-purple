﻿using System.Collections;
using System.Text;
using StockAdvisory.BusinessLayer.Models;
using StockAdvisory.BusinessLayer.Services.Interfaces;

namespace StockAdvisory.BusinessLayer.Services;

public class HtmlExportService : IExportService
{
    public ExportType ExportType => ExportType.Html;

    public Stream Export(FundReport report)
    {
        var stream = new MemoryStream();
        var writer = new StreamWriter(stream);

        writer.Write(
            $"""
            <!DOCTYPE html>
            <html>
            	<head>
            	</head>
            	<body>
            		{CreateHtmlTable(ExportConstants.IncreasedPositionsHeader, report.IncreasedPositions)}
            		{CreateHtmlTable(ExportConstants.DecreasedPositionsHeader, report.DecreasedPositions)}
            		{CreateHtmlTable(ExportConstants.NewPositionsHeader, report.NewPositions)}
            	</body>
            </html>
            """
        );

        writer.Flush();
        stream.Position = 0;
        return stream;
    }

    private static string CreateHtmlTable(string header, ICollection items)
    {
        return $"""
            <h2>{header}</h2>
            		<table>
            			<tr>
            				<th>Company</th>
            				<th>Ticker</th>
            				<th>Shares Diff</th>
            				<th>Weight Diff</th>
            			</tr>{(items.Count > 0 ? $"\n{CreateHtmlTableItems(items)}" : "")}
            		</table>
            """;
    }

    private static string CreateHtmlTableItems(ICollection items)
    {
        StringBuilder sb = new();
        foreach (var record in items)
        {
            var tokens = (record.ToString() ?? "").Split(',');
            sb.AppendLine("\t\t\t<tr>");
            foreach (var token in tokens)
            {
                sb.AppendLine($"\t\t\t\t<td>{token}</td>");
            }
            sb.AppendLine("\t\t\t</tr>");
        }
        return sb.ToString();
    }
}
