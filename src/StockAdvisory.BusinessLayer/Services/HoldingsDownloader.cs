﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using StockAdvisory.BusinessLayer.Exceptions;
using StockAdvisory.BusinessLayer.Services.Interfaces;

namespace StockAdvisory.BusinessLayer.Services;

public class HoldingsDownloader : IHoldingsDownloader
{
    private readonly ILogger<HoldingsDownloader> _logger;
    private readonly HttpClient _client;

    public HoldingsDownloader(
        ILogger<HoldingsDownloader> logger,
        IOptions<FundSettings> options,
        HttpClient client
    )
    {
        _logger = logger;
        _client = client;
        _client.DefaultRequestHeaders.UserAgent.ParseAdd(options.Value.DefaultRequestHeader);
    }

    public async Task<Stream> Download(string url)
    {
        try
        {
            using var response = await _client.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                _logger.LogInformation("File downloaded successfully at {Now}.", DateTime.Now);
                return await CreateStream(response.Content);
            }
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Failed to download file.");
            throw new HoldingsDownloaderException("Failed to download file.");
        }

        _logger.LogError("Failed to download file at {Now}.", DateTime.Now);
        throw new HoldingsDownloaderException("Failed to download file.");
    }

    private static async Task<Stream> CreateStream(HttpContent response)
    {
        var stream = new MemoryStream();
        await response.CopyToAsync(stream);
        stream.Position = 0;
        return stream;
    }
}
