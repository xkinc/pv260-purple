﻿using StockAdvisory.BusinessLayer.Exceptions;
using StockAdvisory.BusinessLayer.Extensions;
using StockAdvisory.BusinessLayer.Services.Interfaces;

namespace StockAdvisory.BusinessLayer.Services;

public class FileStorageService : IStorageService
{
    public async Task SaveData(string savePath, Stream inputStream)
    {
        try
        {
            await using var file = File.Create(savePath);
            await inputStream.CopyToAsync(file);
            inputStream.Position = 0;
        }
        catch
        {
            throw new StorageException($"Failed to save data to {savePath}");
        }
    }

    public async Task<Stream?> LoadData(string path)
    {
        try
        {
            if (!File.Exists(path))
            {
                return null;
            }
            await using var fileStream = File.OpenRead(path);
            return await fileStream.CreateCopyAsync();
        }
        catch
        {
            throw new StorageException($"Failed to load data from {path}");
        }
    }
}
