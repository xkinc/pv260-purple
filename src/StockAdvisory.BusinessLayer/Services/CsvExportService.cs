﻿using System.Collections;
using System.Globalization;
using CsvHelper;
using StockAdvisory.BusinessLayer.Models;
using StockAdvisory.BusinessLayer.Services.Interfaces;

namespace StockAdvisory.BusinessLayer.Services;

public class CsvExportService : IExportService
{
    public ExportType ExportType => ExportType.Csv;

    public Stream Export(FundReport report)
    {
        var stream = new MemoryStream();
        var writer = new StreamWriter(stream);
        var csv = new CsvWriter(writer, CultureInfo.InvariantCulture);

        WriteReportSection(
            csv,
            ExportConstants.IncreasedPositionsHeader,
            report.IncreasedPositions
        );
        WriteReportSection(
            csv,
            ExportConstants.DecreasedPositionsHeader,
            report.DecreasedPositions
        );
        WriteReportSection(csv, ExportConstants.NewPositionsHeader, report.NewPositions);

        csv.Flush();
        stream.Position = 0;
        return stream;
    }

    private static void WriteReportSection(CsvWriter csv, string comment, IEnumerable records)
    {
        csv.WriteComment(comment);
        csv.NextRecord();
        csv.WriteRecords(records);
    }
}
