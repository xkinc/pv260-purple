﻿using System.Net.Mail;
using Microsoft.Extensions.Options;
using StockAdvisory.BusinessLayer.Exceptions;
using StockAdvisory.BusinessLayer.Services.Interfaces;

namespace StockAdvisory.BusinessLayer.Services;

public class EmailSender(ISmtpClient smtpClient, IOptions<EmailConfiguration> emailConfiguration)
    : INotificationSender
{
    public async Task SendNotificationWithAttachment(string[] receivers, Attachment attachment)
    {
        using var mail = new MailMessage();
        mail.Subject = emailConfiguration.Value.Subject;
        mail.Body = emailConfiguration.Value.Body;

        try
        {
            mail.To.Add(string.Join(",", receivers));
        }
        catch (Exception)
        {
            throw new NotificationFormatException("Emails in invalid format");
        }

        mail.Attachments.Add(attachment);
        await smtpClient.SendMailAsync(mail);
    }
}
