﻿using System.Net.Mail;

namespace StockAdvisory.BusinessLayer.Services.Interfaces;

public interface ISmtpClient
{
    /// <summary>
    /// Adds the sender address to the email, then sends it via an SMTP client.
    /// </summary>
    /// <param name="mail">The mail to be sent</param>
    /// <exception cref="NotificationException"></exception>
    Task SendMailAsync(MailMessage mail);
}
