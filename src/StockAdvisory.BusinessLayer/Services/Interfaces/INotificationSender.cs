﻿using System.Net.Mail;

namespace StockAdvisory.BusinessLayer.Services.Interfaces;

public interface INotificationSender
{
    /// <summary>
    /// Sends a notification with the provided attachment to the specified recievers.
    /// </summary>
    /// <param name="receivers">A string array of reciever identificators/addresses</param>
    /// <param name="attachment">The attachment to send with the notification</param>
    /// <returns>True if the operation executed successfully, false otherwise</returns>
    /// <exception cref="NotificationException"></exception>
    Task SendNotificationWithAttachment(string[] receivers, Attachment attachment);
}
