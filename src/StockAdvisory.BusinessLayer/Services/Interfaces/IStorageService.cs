﻿namespace StockAdvisory.BusinessLayer.Services.Interfaces;

public interface IStorageService
{
    /// <summary>
    /// Loads data from storage based on the provided path.
    /// </summary>
    /// <param name="path">The path to the stored data, path format is specific based on the implementation used.</param>
    /// <returns>A stream of data read from the specified path.</returns>
    /// <exception cref="StorageException">Thrown when loading data failed</exception>
    Task<Stream?> LoadData(string path);

    /// <summary>
    /// Saves the data provided as an input parameter to the specified path.
    /// </summary>
    /// <param name="savePath">The path to the stor the data, path format is specific based on the implementation used.</param>
    /// <exception cref="StorageException">Thrown when storing data failed</exception>
    Task SaveData(string savePath, Stream inputStream);
}
