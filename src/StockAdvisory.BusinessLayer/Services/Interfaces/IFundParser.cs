﻿using StockAdvisory.BusinessLayer.Models;

namespace StockAdvisory.BusinessLayer.Services.Interfaces;

public interface IFundParser
{
    /// <summary>
    /// Parses the provided memory stream into funds
    /// </summary>
    /// <param name="stream"></param>
    /// <returns>An ICollection of funds.</returns>
    /// <exception cref="ParsingException"> the exception is thrown when the parsing of the fund data failed</exception>
    ICollection<Fund> ParseFundsFromStream(Stream stream);
}
