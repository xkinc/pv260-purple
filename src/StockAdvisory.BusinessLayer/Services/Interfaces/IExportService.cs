﻿using StockAdvisory.BusinessLayer.Models;

namespace StockAdvisory.BusinessLayer.Services.Interfaces;

public interface IExportService
{
    ExportType ExportType { get; }

    /// <summary>
    /// Exports the provided report. The export format is specified by the service implementation used.
    /// </summary>
    /// <param name="report">The report to be exported</param>
    /// <returns>A stream of data in a format specified by the implementation.</returns>
    Stream Export(FundReport report);
}
