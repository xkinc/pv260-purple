﻿namespace StockAdvisory.BusinessLayer.Services.Interfaces;

public interface IHoldingsDownloader
{
    /// <summary>
    /// Downloads the data from the specified source.
    /// </summary>
    /// <param name="url">The Url to download the requested data from</param>
    /// <returns>A stream representation of the downloaded data</returns>
    /// <exception cref="HoldingsDownloaderException"></exception>
    Task<Stream> Download(string url);
}
