﻿using Microsoft.VisualBasic.FileIO;
using StockAdvisory.BusinessLayer.Exceptions;
using StockAdvisory.BusinessLayer.Models;
using StockAdvisory.BusinessLayer.Services.Interfaces;

namespace StockAdvisory.BusinessLayer.Services;

public class CsvFundParser : IFundParser
{
    public ICollection<Fund> ParseFundsFromStream(Stream stream)
    {
        using TextFieldParser csvParser = new(stream);

        List<Fund> funds = [];

        csvParser.CommentTokens = ["#"];
        csvParser.SetDelimiters(",");
        csvParser.HasFieldsEnclosedInQuotes = true;

        try
        {
            csvParser.ReadLine();

            while (!csvParser.EndOfData)
            {
                var fields = csvParser.ReadFields()!;
                if (Fund.TryParseFund(fields, out var fund))
                {
                    funds.Add(fund);
                }
            }
        }
        catch (Exception e)
        {
            throw new ParsingException(
                "Failed to parse file, file may contain fields out of order or is missing fields",
                e
            );
        }

        return funds;
    }
}
