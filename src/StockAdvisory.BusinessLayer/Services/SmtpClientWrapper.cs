﻿using System.Net;
using System.Net.Mail;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using StockAdvisory.BusinessLayer.Exceptions;
using StockAdvisory.BusinessLayer.Services.Interfaces;

namespace StockAdvisory.BusinessLayer.Services;

public class EmailConfiguration
{
    public string SenderEmail { get; set; } = string.Empty;
    public string SenderPassword { get; set; } = string.Empty;
    public string Subject { get; set; } = string.Empty;
    public string Body { get; set; } = string.Empty;
    public string SmtpAddress { get; set; } = string.Empty;
    public int PortNumber { get; set; }
    public string EnableSSL { get; set; } = string.Empty;
}

public class SmtpClientWrapper : ISmtpClient
{
    private readonly SmtpClient _smtpClient;
    private readonly EmailConfiguration _config;
    private readonly ILogger<SmtpClientWrapper> _logger;

    public SmtpClientWrapper(
        IOptions<EmailConfiguration> options,
        ILogger<SmtpClientWrapper> logger
    )
    {
        _config = options.Value;
        _smtpClient = new(options.Value.SmtpAddress, options.Value.PortNumber)
        {
            UseDefaultCredentials = false,
            Credentials = new NetworkCredential(_config.SenderEmail, _config.SenderPassword),
            EnableSsl = bool.Parse(_config.EnableSSL)
        };
        _logger = logger;
    }

    public async Task SendMailAsync(MailMessage mail)
    {
        mail.From = new MailAddress(_config.SenderEmail);
        try
        {
            await _smtpClient.SendMailAsync(mail);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Failed to send mail");
            throw new NotificationException("Sending notification(s) failed.");
        }
    }
}
