﻿using Cocona;
using Mediator;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StockAdvisory.BusinessLayer;
using StockAdvisory.BusinessLayer.Models;
using StockAdvisory.BusinessLayer.Requests;

var builder = CoconaApp.CreateBuilder();

builder.Configuration.AddJsonFile("appsettings.json");
builder.Configuration.AddUserSecrets<Program>();

builder.Services.AddLogging();
builder.Services.AddBusinessLayerServices(builder.Configuration);

var app = builder.Build();

app.AddCommand(
    "diff",
    async (
        string[] emails,
        string oldFilePath,
        string newFilePath,
        IMediator mediator,
        ExportType exportType = ExportType.Html
    ) =>
    {
        var result = await mediator.Send(
            new RunDiffReportRequest(emails, oldFilePath, newFilePath, exportType)
        );
        if (result.HasError)
        {
            Console.WriteLine($"Errors: {string.Join(", ", result.ErrorMessages)}");
        }
        return result.ErrorCode;
    }
);

app.AddCommand(
    "download",
    async (string saveFilePath, string? holdingsUrl, IMediator mediator) =>
    {
        var result = await mediator.Send(new DownloadHoldingsRequest(saveFilePath, holdingsUrl));
        if (result.HasError)
        {
            Console.WriteLine($"Errors: {string.Join(", ", result.ErrorMessages)}");
        }
        return result.ErrorCode;
    }
);

app.Run();
