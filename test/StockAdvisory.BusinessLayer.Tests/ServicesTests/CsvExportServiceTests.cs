﻿using StockAdvisory.BusinessLayer.Models;
using StockAdvisory.BusinessLayer.Services;

namespace StockAdvisory.BusinessLayer.Tests.ServicesTests;

public class CsvExportServiceTests
{
    private readonly CsvExportService _exportService = new();

    [Fact]
    public void TestExportCsv()
    {
        var report = new FundReport();
        report.IncreasedPositions.Add(
            new FundDiff(DateTime.Now, "Fund1", "Company1", "Ticker1", "Cusip1", 10, 1000m, 0.1m)
        );

        using var stream = _exportService.Export(report);
        using var reader = new StreamReader(stream);

        var result = reader.ReadToEnd();

        Assert.Contains("Increased Positions:", result);
        Assert.Contains("Fund1,Company1,Ticker1,Cusip1,10,1000,0.1", result);
    }
}
