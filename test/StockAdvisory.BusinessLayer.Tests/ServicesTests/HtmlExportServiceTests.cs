﻿using StockAdvisory.BusinessLayer.Models;
using StockAdvisory.BusinessLayer.Services;

namespace StockAdvisory.BusinessLayer.Tests.ServicesTests;

public class HtmlExportServiceTests
{
    private readonly HtmlExportService _exportService = new();

    [Fact]
    public void TestExportHtmlStream()
    {
        var report = new FundReport();
        report.IncreasedPositions.Add(
            new FundDiff(DateTime.Now, "Fund1", "Company1", "Ticker1", "Cusip1", 10, 1000m, 0.1m)
        );

        using var stream = _exportService.Export(report);
        using var reader = new StreamReader(stream);

        var result = reader.ReadToEnd();

        Assert.Contains("<!DOCTYPE html>", result);
        Assert.Contains("<html>", result);
        Assert.Contains("</html>", result);
        Assert.Contains("Increased Positions:", result);
        Assert.Contains("<td>Company1</td>", result);
        Assert.Contains("<td>Ticker1</td>", result);
        Assert.Contains("<td>0.1</td>", result);
        Assert.Contains("<td>10</td>", result);
    }

    [Fact]
    public void TestExportHtmlStream_NoDiffs()
    {
        var report = new FundReport();

        using var stream = _exportService.Export(report);
        using var reader = new StreamReader(stream);

        var result = reader.ReadToEnd();

        Assert.Contains("<!DOCTYPE html>", result);
        Assert.Contains("<html>", result);
        Assert.Contains("</html>", result);
    }
}
