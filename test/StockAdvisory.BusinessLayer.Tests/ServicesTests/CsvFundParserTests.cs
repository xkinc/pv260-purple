﻿using System.Text;
using StockAdvisory.BusinessLayer.Exceptions;
using StockAdvisory.BusinessLayer.Services;

namespace StockAdvisory.BusinessLayer.Tests.ServicesTests;

public class CsvFundParserTests
{
    private readonly CsvFundParser _csvFundParser = new();

    [Fact]
    public void LoadFromStream_ShouldLoadFunds()
    {
        var csvData = new StringBuilder();
        csvData.AppendLine("Date,FundName,Company,Ticker,Cusip,Shares,MarketValue,Weight");
        csvData.AppendLine("2024-03-01,Fund1,Company1,Ticker1,Cusip1,10,1000,0.1");

        using var stream = new MemoryStream(Encoding.UTF8.GetBytes(csvData.ToString()));

        var funds = _csvFundParser.ParseFundsFromStream(stream).ToList();

        Assert.Single(funds);
        Assert.Equal("Fund1", funds[0].FundName);
    }

    [Fact]
    public void LoadFromStream_FailsOnMalformedCSV()
    {
        var csvData = new StringBuilder();
        csvData.AppendLine("Date,FundName,Company,Ticker,Cusip,Shares,MarketValue,Weight");
        csvData.AppendLine("2024-03-01");

        using var stream = new MemoryStream(Encoding.UTF8.GetBytes(csvData.ToString()));

        Assert.Throws<ParsingException>(() => _csvFundParser.ParseFundsFromStream(stream));
    }
}
