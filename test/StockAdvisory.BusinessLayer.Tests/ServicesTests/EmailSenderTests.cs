﻿using System.Net.Mail;
using System.Net.Mime;
using Microsoft.Extensions.Options;
using Moq;
using StockAdvisory.BusinessLayer.Exceptions;
using StockAdvisory.BusinessLayer.Services;
using StockAdvisory.BusinessLayer.Services.Interfaces;

namespace StockAdvisory.BusinessLayer.Tests.ServicesTests;

public class EmailSenderTests
{
    private readonly Mock<ISmtpClient> _smtpClientMock;
    private readonly EmailSender _emailSender;

    public EmailSenderTests()
    {
        _smtpClientMock = new Mock<ISmtpClient>();
        _smtpClientMock
            .Setup(s => s.SendMailAsync(It.IsAny<MailMessage>()))
            .Returns(Task.CompletedTask);

        Mock<IOptions<EmailConfiguration>> mockEmailOptions = new();

        mockEmailOptions
            .Setup(m => m.Value)
            .Returns(new EmailConfiguration() { Subject = "Test subject", Body = "Test body" });
        _emailSender = new EmailSender(_smtpClientMock.Object, mockEmailOptions.Object);
    }

    [Fact]
    public async Task SendNotificationWithAttachment_ShouldBeCorrect_WhenValidEmailProvided()
    {
        using var attachment = new Attachment(
            new MemoryStream(),
            "test.html",
            MediaTypeNames.Text.Html
        );

        await _emailSender.SendNotificationWithAttachment(["test@example.com"], attachment);

        _smtpClientMock.Verify(s => s.SendMailAsync(It.IsAny<MailMessage>()), Times.Once);
    }

    [Theory]
    [InlineData]
    [InlineData("")]
    [InlineData("invalid email")]
    [InlineData("valid@mail.com", "invalid email")]
    public async Task SendNotificationWithAttachment_ShouldThrowException_WhenInvalidEmailsProvided(
        params string[] emails
    )
    {
        using var attachment = new Attachment(
            new MemoryStream(),
            "test.html",
            MediaTypeNames.Text.Html
        );

        await Assert.ThrowsAsync<NotificationFormatException>(
            () => _emailSender.SendNotificationWithAttachment(emails, attachment)
        );

        _smtpClientMock.Verify(s => s.SendMailAsync(It.IsAny<MailMessage>()), Times.Never);
    }
}
