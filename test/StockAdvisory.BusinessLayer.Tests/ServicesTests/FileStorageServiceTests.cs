﻿using System.Text;
using StockAdvisory.BusinessLayer.Exceptions;
using StockAdvisory.BusinessLayer.Services;

namespace StockAdvisory.BusinessLayer.Tests.ServicesTests;

public class FileStorageServiceTests
{
    private readonly FileStorageService _storageService = new();

    [Fact]
    public async Task SaveData_ShouldSucceed()
    {
        const string saveFilePath = "test_save_data.txt";
        const string testData = "Test data.";

        var inputStream = new MemoryStream(Encoding.UTF8.GetBytes(testData));

        await _storageService.SaveData(saveFilePath, inputStream);

        Assert.True(File.Exists(saveFilePath));
        await using (var fileStream = File.OpenRead(saveFilePath))
        using (var reader = new StreamReader(fileStream))
        {
            var savedData = await reader.ReadToEndAsync();
            Assert.Equal(testData, savedData);
        }

        File.Delete(saveFilePath);
    }

    [Fact]
    public async Task SaveData_ShouldFail_WhenFileIsInUse()
    {
        const string saveFilePath = "test.txt";

        // Keep file open
        await using (File.Create(saveFilePath))
        {
            await Assert.ThrowsAsync<StorageException>(async () =>
            {
                const string testData = "Test data.";
                var inputStream = new MemoryStream(Encoding.UTF8.GetBytes(testData));

                // Save data to a file in use
                await _storageService.SaveData(saveFilePath, inputStream);
            });
        }
    }

    [Fact]
    public async Task LoadData_ShouldReturnStream_WhenGivenExistingFile()
    {
        const string saveFilePath = "test_load_data.txt";
        const string testData = "Test data.";
        await File.WriteAllTextAsync(saveFilePath, testData);

        var stream = await _storageService.LoadData(saveFilePath);

        Assert.NotNull(stream);
        Assert.True(File.Exists(saveFilePath));
        using (var reader = new StreamReader(stream))
        {
            var loadedData = await reader.ReadToEndAsync();
            Assert.Equal(testData, loadedData);
        }

        File.Delete(saveFilePath);
    }

    [Fact]
    public async Task LoadData_ShouldReturnNull_WhenGivenNonExistingFile()
    {
        const string saveFilePath = "non_existing_file.txt";

        var stream = await _storageService.LoadData(saveFilePath);

        Assert.Null(stream);
    }

    [Fact]
    public async Task LoadData_ShouldFailWhenFileIsInUse()
    {
        const string saveFilePath = "test.txt";

        // Keep file open
        await using (File.Create(saveFilePath))
        {
            await Assert.ThrowsAsync<StorageException>(async () =>
            {
                // Load from file in use
                await _storageService.LoadData(saveFilePath);
            });
        }
    }
}
