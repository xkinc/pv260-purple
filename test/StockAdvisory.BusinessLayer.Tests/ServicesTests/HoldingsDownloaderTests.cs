﻿using System.Net;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using StockAdvisory.BusinessLayer.Exceptions;
using StockAdvisory.BusinessLayer.Services;
using StockAdvisory.BusinessLayer.Tests.Extensions;

namespace StockAdvisory.BusinessLayer.Tests.ServicesTests;

public class HoldingsDownloaderTests
{
    private readonly Mock<ILogger<HoldingsDownloader>> _mockLogger = new();
    private readonly Mock<IOptions<FundSettings>> _mockOptions;

    public HoldingsDownloaderTests()
    {
        _mockOptions = new Mock<IOptions<FundSettings>>();
        _mockOptions
            .Setup(m => m.Value)
            .Returns(
                new FundSettings()
                {
                    DefaultRequestHeader =
                        "Mozilla/5.0 (Windows NT 10.0; Win64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3"
                }
            );
    }

    [Fact]
    public async Task Download_ShouldSucceed_WhenOkStatusCode()
    {
        const string csvData =
            "Date,FundName,Company,Ticker,Cusip,Shares,MarketValue,Weight\n2024-03-01,Fund1,Company1,Ticker1,Cusip1,10,1000,0.1";
        var mockHttpMessageHandler = new MockHttpMessageHandler(csvData, HttpStatusCode.OK);

        var httpClient = new HttpClient(mockHttpMessageHandler);
        var holderDownloader = new HoldingsDownloader(
            _mockLogger.Object,
            _mockOptions.Object,
            httpClient
        );

        await using var stream = await holderDownloader.Download("https://example.com/");
        Assert.NotNull(stream);

        _mockLogger.VerifyLogging(LogLevel.Information, Times.Once());
    }

    [Fact]
    public async Task Download_ShouldFail_WhenNotSuccessStatusCode()
    {
        var mockHttpMessageHandler = new MockHttpMessageHandler("Error", HttpStatusCode.BadRequest);

        var httpClient = new HttpClient(mockHttpMessageHandler);
        var holderDownloader = new HoldingsDownloader(
            _mockLogger.Object,
            _mockOptions.Object,
            httpClient
        );

        await Assert.ThrowsAsync<HoldingsDownloaderException>(
            async () => await holderDownloader.Download("https://example.com/")
        );

        _mockLogger.VerifyLogging(LogLevel.Error, Times.Once());
    }

    [Fact]
    public async Task Download_ShouldFail_WhenHttpClientThrowsException()
    {
        var mockHttpMessageHandler = new MockHttpMessageHandler("Error", HttpStatusCode.BadRequest);

        var httpClient = new HttpClient(mockHttpMessageHandler);
        var holderDownloader = new HoldingsDownloader(
            _mockLogger.Object,
            _mockOptions.Object,
            httpClient
        );

        await Assert.ThrowsAsync<HoldingsDownloaderException>(
            async () => await holderDownloader.Download("invalid format")
        );

        _mockLogger.VerifyLogging(LogLevel.Error, Times.Once());
    }
}

public class MockHttpMessageHandler(string response, HttpStatusCode statusCode) : HttpMessageHandler
{
    protected override async Task<HttpResponseMessage> SendAsync(
        HttpRequestMessage request,
        CancellationToken cancellationToken
    )
    {
        return await Task.Run(
            () =>
                new HttpResponseMessage
                {
                    StatusCode = statusCode,
                    Content = new StringContent(response)
                },
            cancellationToken
        );
    }
}
