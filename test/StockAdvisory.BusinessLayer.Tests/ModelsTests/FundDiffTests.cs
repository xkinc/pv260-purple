using StockAdvisory.BusinessLayer.Models;

namespace StockAdvisory.BusinessLayer.Tests.ModelsTests;

public class FundDiffTests
{
    [Fact]
    public void ToString_ShouldReturnStringInExpectedFormat()
    {
        var fundDiff = new FundDiff(
            DateTime.Now,
            "FundName",
            "Company",
            "Ticker",
            "Cusip",
            10,
            1000m,
            0.1m
        );

        var result = fundDiff.ToString();

        Assert.Equal("Company,Ticker,10,0.1", result);
    }

    [Fact]
    public void GetFundDiff_ShouldCalculateDiffValues()
    {
        var oldFund = new Fund(DateTime.Now, "Fund", "Company", "Ticker", "Cusip", 10, 1000m, 0.1m);

        var newFund = new Fund(DateTime.Now, "Fund", "Company", "Ticker", "Cusip", 20, 2000m, 0.2m);

        var fundDiff = FundDiff.GetFundDiff(oldFund, newFund);

        Assert.Equal(10, fundDiff.SharesDiff);
        Assert.Equal(1000m, fundDiff.MarketValueDiff);
        Assert.Equal(0.1m, fundDiff.WeightDiff);
    }

    [Fact]
    public void GetFundDiff_ShouldThrowsException_WhenCusipDiffers()
    {
        var oldFund = new Fund(DateTime.Now, cusip: "OldCusip");
        var newFund = new Fund(DateTime.Now, cusip: "NewCusip");

        Assert.Throws<ArgumentException>(() => FundDiff.GetFundDiff(oldFund, newFund));
    }
}
