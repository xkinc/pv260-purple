﻿using System.Globalization;
using StockAdvisory.BusinessLayer.Models;

namespace StockAdvisory.BusinessLayer.Tests.ModelsTests;

public class FundTests
{
    [Fact]
    public void ToString_ShouldReturnExpectedString()
    {
        var culture = CultureInfo.GetCultureInfo("en-US");
        const string dateString = "04/21/2022 00:00:00";
        DateTime.TryParse(dateString, culture, out var date);

        var fund = new Fund(
            date,
            "Test Fund",
            "Test Company",
            "TEST",
            "123456789",
            100,
            5000.00m,
            0.1m
        );

        var result = fund.ToString();

        var expected = $"{date},Test Fund,Test Company,TEST,123456789,100,5000.00,0.1";
        Assert.Equal(expected, result);
    }

    [Theory]
    [InlineData(
        "04/21/2022 12:00:00 AM",
        "Test Fund",
        "Test Company",
        "TEST",
        "123456789",
        "100",
        "5000.00",
        "10.1%"
    )]
    public void TryParseFund_ShouldParseValidInput_ReturnsTrue(
        string date,
        string fundName,
        string company,
        string ticker,
        string cusip,
        string shares,
        string marketValue,
        string weight
    )
    {
        string[] fields = [date, fundName, company, ticker, cusip, shares, marketValue, weight];

        var result = Fund.TryParseFund(fields, out var fund);

        Assert.True(result);
        Assert.Equal(
            date,
            fund.Date.ToString("MM/dd/yyyy hh:mm:ss tt", CultureInfo.GetCultureInfo("en-US"))
        );
        Assert.Equal(fundName, fund.FundName);
        Assert.Equal(company, fund.Company);
        Assert.Equal(ticker, fund.Ticker);
        Assert.Equal(cusip, fund.Cusip);
        Assert.Equal(int.Parse(shares), fund.Shares);
        Assert.Equal(decimal.Parse(marketValue), fund.MarketValue);
        Assert.Equal(decimal.Parse(weight.TrimEnd('%')), fund.Weight);
    }

    [Theory]
    [InlineData(
        "invalid_date",
        "Test Fund",
        "Test Company",
        "TEST",
        "123456789",
        "100",
        "5000.00",
        "10.1%"
    )]
    [InlineData(
        "4/21/2022",
        "Test Fund",
        "Test Company",
        "TEST",
        "123456789",
        "not_an_integer",
        "5000.00",
        "10.1%"
    )]
    [InlineData(
        "4/21/2022",
        "Test Fund",
        "Test Company",
        "TEST",
        "123456789",
        "100",
        "not_a_decimal",
        "10.1%"
    )]
    [InlineData(
        "4/21/2022",
        "Test Fund",
        "Test Company",
        "TEST",
        "123456789",
        "100",
        "5000.00",
        "not_a_decimal"
    )]
    public void TryParseFund_ShouldFailToParseInvalidInput_ReturnsFalse(params string[] fields)
    {
        var result = Fund.TryParseFund(fields, out _);

        Assert.False(result);
    }
}
