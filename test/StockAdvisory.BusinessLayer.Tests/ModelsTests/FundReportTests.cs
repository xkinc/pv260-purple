﻿using StockAdvisory.BusinessLayer.Models;

namespace StockAdvisory.BusinessLayer.Tests.ModelsTests;

public class FundReportTests
{
    [Fact]
    public void CreateReport_ShouldProduceNoChanges()
    {
        var oldFunds = new List<Fund>
        {
            new(DateTime.Now, cusip: "Cusip1", shares: 100),
            new(DateTime.Now, cusip: "Cusip2", shares: 200)
        };

        var newFunds = new List<Fund>
        {
            new(DateTime.Now, cusip: "Cusip1", shares: 100),
            new(DateTime.Now, cusip: "Cusip2", shares: 200)
        };

        var report = FundReport.CreateReport(oldFunds, newFunds);

        Assert.Empty(report.NewPositions);
        Assert.Empty(report.RemovedPositions);
        Assert.Empty(report.IncreasedPositions);
        Assert.Empty(report.DecreasedPositions);
    }

    [Fact]
    public void CreateReport_ShouldProduceRemovedPositions()
    {
        var oldFunds = new List<Fund>
        {
            new(DateTime.Now, cusip: "Cusip1", shares: 100),
            new(DateTime.Now, cusip: "Cusip2", shares: 200)
        };

        var newFunds = new List<Fund> { new(DateTime.Now, cusip: "Cusip1", shares: 100) };

        var report = FundReport.CreateReport(oldFunds, newFunds);

        Assert.Empty(report.NewPositions);
        Assert.Single(report.RemovedPositions);
        Assert.Equal("Cusip2", report.RemovedPositions[0].Cusip);
        Assert.Empty(report.IncreasedPositions);
        Assert.Empty(report.DecreasedPositions);
    }

    [Fact]
    public void CreateReport_ShouldProduceIncreasedAndDecreasedPositions()
    {
        var oldFunds = new List<Fund>
        {
            new(DateTime.Now, cusip: "Cusip1", shares: 100),
            new(DateTime.Now, cusip: "Cusip2", shares: 200)
        };

        var newFunds = new List<Fund>
        {
            new(DateTime.Now, cusip: "Cusip1", shares: 150),
            new(DateTime.Now, cusip: "Cusip2", shares: 100)
        };

        var report = FundReport.CreateReport(oldFunds, newFunds);

        Assert.Empty(report.NewPositions);
        Assert.Empty(report.RemovedPositions);
        Assert.Single(report.IncreasedPositions);
        Assert.Equal("Cusip1", report.IncreasedPositions[0].Cusip);
        Assert.Single(report.DecreasedPositions);
        Assert.Equal("Cusip2", report.DecreasedPositions[0].Cusip);
    }

    [Fact]
    public void CreateReport_ShouldProduceNewPositions()
    {
        var oldFunds = new List<Fund> { new(DateTime.Now, cusip: "Cusip1", shares: 100) };

        var newFunds = new List<Fund>
        {
            new(DateTime.Now, cusip: "Cusip1", shares: 100),
            new(DateTime.Now, cusip: "Cusip2", shares: 200)
        };

        var report = FundReport.CreateReport(oldFunds, newFunds);

        Assert.Single(report.NewPositions);
        Assert.Equal("Cusip2", report.NewPositions[0].Cusip);
        Assert.Empty(report.RemovedPositions);
        Assert.Empty(report.IncreasedPositions);
        Assert.Empty(report.DecreasedPositions);
    }
}
