﻿using Microsoft.Extensions.Logging;
using Moq;

namespace StockAdvisory.BusinessLayer.Tests.Extensions;

public static class LoggerExtensions
{
    public static void VerifyLogging<T>(
        this Mock<ILogger<T>> loggerMock,
        LogLevel logLevel,
        Times times
    )
    {
        loggerMock.Verify(
            x =>
                x.Log(
                    logLevel,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((_, _) => true),
                    It.IsAny<Exception>(),
                    It.Is<Func<It.IsAnyType, Exception, string>>((_, _) => true)!
                ),
            times
        );
    }
}
