﻿using Microsoft.Extensions.DependencyInjection;
using StockAdvisory.BusinessLayer.Requests;

namespace StockAdvisory.BusinessLayer.Tests.RequestHandlerTests;

public class DownloadHoldingsRequestHandlerTests(AppFixture fixture) : IClassFixture<AppFixture>
{
    [Fact]
    public async Task Handle_ShouldSucceed()
    {
        // Arrange
        var handler = fixture.Services.GetRequiredService<DownloadHoldingsRequestHandler>();
        var filePath = Path.GetTempFileName();

        // Act
        var result = await handler.Handle(
            new DownloadHoldingsRequest(filePath, "http://test.com"),
            default
        );

        // Assert
        Assert.False(result.HasError);
        var text = File.ReadAllText(Configuration.directoryPath + Path.GetFileName(filePath));
        Assert.Contains("Date,FundName,Company,Ticker,Cusip,Shares,MarketValue,Weight", text);
        Assert.Contains("2024-03-01,Fund1,Company1,Ticker1,Cusip1,10,1000,0.1", text);
    }

    [Fact]
    public async Task Handle_ShouldFail_WhenSomeErrorOccurs()
    {
        // Arrange
        var handler = fixture.Services.GetRequiredService<DownloadHoldingsRequestHandler>();
        var filePath = Path.GetTempFileName();

        // Act
        var result = await handler.Handle(new DownloadHoldingsRequest(filePath, "test"), default);

        // Assert
        Assert.True(result.HasError);
        Assert.Equal("error", result.ErrorMessages[0]);
    }
}
