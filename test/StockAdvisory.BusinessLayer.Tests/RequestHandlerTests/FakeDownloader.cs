﻿using System.Text;
using StockAdvisory.BusinessLayer.Exceptions;
using StockAdvisory.BusinessLayer.Services.Interfaces;

namespace StockAdvisory.BusinessLayer.Tests.RequestHandlerTests;

public class FakeDownloader : IHoldingsDownloader
{
    public Task<Stream> Download(string url)
    {
        if (url == "test")
        {
            throw new HoldingsDownloaderException("error");
        }
        const string csvData =
            "Date,FundName,Company,Ticker,Cusip,Shares,MarketValue,Weight\n2024-03-01,Fund1,Company1,Ticker1,Cusip1,10,1000,0.1";
        return Task.FromResult<Stream>(new MemoryStream(Encoding.UTF8.GetBytes(csvData)));
    }
}
