﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using StockAdvisory.BusinessLayer.Services.Interfaces;

namespace StockAdvisory.BusinessLayer.Tests.RequestHandlerTests;

public class AppFixture : IDisposable
{
    private readonly IHost host;

    public IServiceProvider Services => host.Services;

    public AppFixture()
    {
        host = new HostBuilder()
            .ConfigureServices(services =>
            {
                var configuration = new ConfigurationBuilder().Build();
                services.AddLogging();
                services.AddBusinessLayerServices(configuration);

                services.RemoveAll(typeof(IHoldingsDownloader));
                services.AddSingleton<IHoldingsDownloader, FakeDownloader>();
            })
            .Build();
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
        host.Dispose();
    }
}
