# pv260-purple

## Getting started

Setup secrets in your IDE or using dotnet CLI. \
[https://learn.microsoft.com/en-us/aspnet/core/security/app-secrets?view=aspnetcore-8.0&tabs=windows](https://learn.microsoft.com/en-us/aspnet/core/security/app-secrets?view=aspnetcore-8.0&tabs=windows)

## Usage of `download` command

Usage: `dotnet run -- download [--save-file-path <String>] [--holdings-url <String>] [--help]`

Options:
- `--save-file-path <String>` (Required)
- `--holdings-url <String>`
- `-h`, `--help`

Example: \
``` dotnet run -- download --save-file-path .\data\result\now.csv ```

## Usage of `diff` command

Usage: `dotnet run -- diff [--emails <String>...] [--old-file-path <String>] [--new-file-path <String>] [--export-type <ExportType>] [--help]`

Options:
- `--emails <String>` (Required)
- `--old-file-path <String>` (Required)
- `--new-file-path <String>` (Required)
- `--export-type <ExportType>` (Default: Html) (Allowed values: Html, Csv)
- `-h`, `--help`

Example: \
``` dotnet run -- diff --emails filip@gmail.com --emails thomas@gmail.com --old-file-path .\data\result\holdings-test.csv --new-file-path .\data\result\now.csv ```
